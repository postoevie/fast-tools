//
//  PSToolSettingsRepository.swift
//  FastTools
//
//  Created by Igor Postoev on 13.03.2022.
//

import Foundation

class PSToolSettingsRepository: ToolSettingsRepository {

  let dataStore: ToolDatastore
  // TODO: add remote

  init(store: ToolDatastore) {
    self.dataStore = store
  }

  func update(settings: [ToolSetting], with name: String) {
    dataStore.set(settings: settings, with: name)
  }

  func getSettings(by name: String) -> [ToolSetting] {
    return dataStore.getSettings(by: name)
  }
}
