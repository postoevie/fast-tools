//
//  ToolDatastore.swift
//  FastTools
//
//  Created by Igor Postoev on 13.03.2022.
//

protocol ToolDatastore {

  func getSettings(by name: String) -> [ToolSetting]
  func set(settings: [ToolSetting], with name: String)
}
