//
//  PSDefaultsToolsDataStore.swift
//  FastTools
//
//  Created by Igor Postoev on 13.03.2022.
//

import Foundation

class PSDefaultsToolsDatastore: ToolDatastore {

  private let defaultSettings: [String: [ToolSetting]] = {
    // TODO: move to userdefaults default values?
    // swiftlint:disable line_length
    return [Tool.camera.rawValue: [ToolSetting(key: "order", value: "1", label: "Order", type: .text, visible: false),
                                   ToolSetting(key: "name", value: "Name", label: "Name", type: .text, visible: false),
                                   ToolSetting(key: "color", value: "#fcc603", label: "Color", type: .color),
                                   ToolSetting(key: "apply_immmidiately", value: "0", label: "Apply immidiately", type: .bool)],
            Tool.envelope.rawValue: [ToolSetting(key: "order", value: "2", label: "Order", type: .text, visible: false),
                                     ToolSetting(key: "name", value: "Name", label: "Name", type: .text, visible: false),
                                     ToolSetting(key: "color", value: "#fcc603", label: "Color", type: .color),
                                     ToolSetting(key: "apply_immmidiately", value: "0", label: "Apply immidiately",  type: .bool)],
            Tool.phoneCall.rawValue: [ToolSetting(key: "order", value: "3", label: "Order", type: .text, visible: false),
                                      ToolSetting(key: "name", value: "Name", label: "Name", type: .text, visible: false),
                                      ToolSetting(key: "color", value: "#fcc603", label: "Color", type: .color),
                                      ToolSetting(key: "apply_immmidiately", value: "0", label: "Apply immidiately", type: .bool)],
            Tool.speaker.rawValue: [ToolSetting(key: "order", value: "4", label: "Order", type: .text, visible: false),
                                    ToolSetting(key: "name", value: "Name", label: "Name", type: .text, visible: false),
                                    ToolSetting(key: "color", value: "#fcc603", label: "Color", type: .color),
                                    ToolSetting(key: "apply_immmidiately", value: "0", label: "Apply immidiately", type: .bool)],
            Tool.voiceRecorder.rawValue: [ToolSetting(key: "order", value: "5", label: "Order", type: .text, visible: false),
                                          ToolSetting(key: "name", value: "Name", label: "Name", type: .text, visible: false),
                                          ToolSetting(key: "color", value: "#fcc603", label: "Color", type: .color),
                                          ToolSetting(key: "apply_immmidiately", value: "0", label: "Apply immidiately", type: .bool)],
            Tool.phoneMessage.rawValue: [ToolSetting(key: "order", value: "6", label: "Order", type: .text, visible: false),
                                         ToolSetting(key: "name", value: "Name", label: "Name", type: .text, visible: false),
                                         ToolSetting(key: "color", value: "#fcc603", label: "Color", type: .color),
                                         ToolSetting(key: "apply_immmidiately", value: "0", label: "Apply immidiately", type: .bool)],
            Tool.flashlight.rawValue:  [ToolSetting(key: "order", value: "7", label: "Order", type: .text, visible: false),
                                        ToolSetting(key: "name", value: "Name", label: "Name", type: .text, visible: false),
                                        ToolSetting(key: "color", value: "#fcc603", label: "Color", type: .color),
                                        ToolSetting(key: "apply_immmidiately", value: "0", label: "Apply immidiately", type: .bool)]]
    // swiftlint:enable line_length
  }()

  func getSettings(by name: String) -> [ToolSetting] {
    if let data = UserDefaults().data(forKey: name) {
      return (try? JSONDecoder().decode([ToolSetting].self, from: data)) ?? []
    }
    // TODO: if no data by tool name then persist default setting - need to check internet api
    if let defaultSetting = defaultSettings[name] {
      set(settings: defaultSetting, with: name)
      return getSettings(by: name)
    }
    return []
  }

  func set(settings: [ToolSetting], with name: String) {
    guard let encoded = try? JSONEncoder().encode(settings) else {
      return
    }
    UserDefaults().set(encoded, forKey: name)
  }
}
