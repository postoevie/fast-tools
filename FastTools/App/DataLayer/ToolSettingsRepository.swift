//
//  ToolSettingsRepository.swift
//  FastTools
//
//  Created by Igor Postoev on 13.03.2022.
//

protocol ToolSettingsRepository {

  func getSettings(by name: String) -> [ToolSetting]
  func update(settings: [ToolSetting], with name: String)
}
