//
//  Tool.swift
//  FastTools
//
//  Created by Igor Postoev on 10.05.2022.
//

enum Tool: String, CaseIterable {
  case camera
  case envelope
  case phoneCall = "phone_call"
  case speaker
  case voiceRecorder = "voice_recorder"
  case phoneMessage = "phone_message"
  case flashlight
}
