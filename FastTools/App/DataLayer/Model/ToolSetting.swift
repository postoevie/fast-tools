//
//  ToolSetting.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

// simulate dictionary behavior
extension Array where Element == ToolSetting {

  subscript(key: String) -> ToolSetting? {

    get {
      return first { $0.key == key }
    }

    set {
      if let index = firstIndex(where: { $0.key == newValue?.key }),
         let newEntry = newValue {
         self[index] = newEntry
      }
    }
  }
}

enum SettingType: String, Codable {
  case bool
  case number
  case text
  case color
}

struct ToolSetting: Codable {
  var key: String
  var value: String
  var label: String
  var type: SettingType
  var visible: Bool = true
}
