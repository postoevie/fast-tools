//
//  PSFlashlightViewController.swift
//  FastTools
//
//  Created by Igor Postoev on 19.06.2022.
//

import AVFoundation

protocol ApplyingToolResponder {
  func apply()
}

class PSFlashlightCoordinator: ApplyingToolResponder {

  var settingsProvider: PSToolSettingsRepository!

  func apply() {
    toggleTorch()
  }

  private func toggleTorch() {
    guard let device = AVCaptureDevice.default(for: .video) else {
      return
    }

    // assume torch'll disabled when become unavailable
    guard device.hasTorch &&
          device.isTorchAvailable &&
          device.isTorchModeSupported(.on) else {
      return
    }

    do {
      try device.lockForConfiguration()
      device.torchMode = device.torchMode == .off ? .on : .off
      device.unlockForConfiguration()
    } catch let error as NSError {
      // TODO: handle errors log
      debugPrint("Torch toggle failed with message: \(error.localizedDescription)")
    }
  }
}
