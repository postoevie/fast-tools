//
//  ToolsApplyingTapResponder.swift
//  FastTools
//
//  Created by Igor Postoev on 10.05.2022.
//

import Foundation

protocol ToolsOverviewDataSource {

  func getTools() -> [Tool]
  func selected(in index: Int)
  func drillToDetails(in index: Int)
  func moved(fromIndex: Int, toIndex: Int)
}
