//
//  PSToolsOverviewViewModel.swift
//  FastTools
//
//  Created by Igor Postoev on 08.03.2022.
//

import Foundation

struct OverviewItem {
  let imageName: String
  let title: String
}

class PSToolsOverviewViewModel {

  private static let itemsByTools: [Tool: OverviewItem] = [
    .camera: OverviewItem(imageName: "camera", title: "Camera"),
      .envelope: OverviewItem(imageName: "envelope", title: "Message"),
      .phoneCall: OverviewItem(imageName: "phone", title: "Call"),
      .speaker: OverviewItem(imageName: "speaker", title: "Speaker"),
      .voiceRecorder: OverviewItem(imageName: "voice_recorder", title: "Recorder"),
      .phoneMessage: OverviewItem(imageName: "phone_message", title: "Voice Message"),
      .flashlight: OverviewItem(imageName: "flashlight", title: "Flashlight")
  ]

  private let dataSource: ToolsOverviewDataSource

  init(dataSource: ToolsOverviewDataSource) {
    self.dataSource = dataSource
  }

  func getItemsCount() -> Int {
    return dataSource.getTools().count
  }

  func getItem(index: Int) -> OverviewItem? {
    let tool = dataSource.getTools()[index]
    return Self.itemsByTools[tool]
  }

  func moved(_ fromIndex: Int,_ toIndex: Int) {
    dataSource.moved(fromIndex: fromIndex, toIndex: toIndex)
  }

  func selected(in index: Int) {
    dataSource.selected(in: index)
  }

  func drillToDetails(in index: Int) {
    dataSource.drillToDetails(in: index)
  }
}
