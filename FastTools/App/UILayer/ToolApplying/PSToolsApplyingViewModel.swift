//
//  PSToolsViewModel.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import Combine
import Foundation

class PSToolsApplyingViewModel: ToolsOverviewDataSource,
                                ToolDetailsUpdateResponder {

  @Published
  var viewState: ApplyingToolsViewState = .overview

  var applyingRespondersByTools: [Tool: ApplyingToolResponder] = [:]
  let settingsRepository: ToolSettingsRepository

  init(repository: ToolSettingsRepository) {
    self.settingsRepository = repository
    // refactor it later
    applyingRespondersByTools[.flashlight] = PSFlashlightCoordinator()
  }

  // MARK: - ToolsOverviewDataSource

  func getTools() -> [Tool] {
    return getOrderedTools()
  }

  private func getOrderedTools() -> [Tool] {
    var settingsByTools: [Tool: [ToolSetting]] = [:]
    Tool.allCases.forEach { tool in
      settingsByTools[tool] = settingsRepository.getSettings(by: tool.rawValue)
    }
    let tools = Array(settingsByTools.keys).sorted { tool1, tool2 in
      let order1 = settingsByTools[tool1]!["order"]?.value ?? "0"
      let order2 = settingsByTools[tool2]!["order"]?.value ?? "0"
      return order1 < order2
    }
    return tools
  }

  func selected(in index: Int) {
    let tool = getTools()[index]
    let settings = settingsRepository.getSettings(by: tool.rawValue)
    if let applyImmidiately = settings["apply_immmidiately"]?.value,
       Bool(applyImmidiately) ?? false {
      applyingRespondersByTools[tool]?.apply()
      return
    }
    viewState = .details(tool)
  }

  func drillToDetails(in index: Int) {
    let tool = getTools()[index]
    viewState = .details(tool)
  }

  func moved(fromIndex: Int, toIndex: Int) {
    var tools = getTools()
    let toolToMove = tools[fromIndex]
    tools.remove(at: fromIndex)
    tools.insert(toolToMove, at: toIndex)
    tools.forEach { tool in
      var settings = settingsRepository.getSettings(by: tool.rawValue)
      if let index = tools.firstIndex(of: tool) {
        settings["order"]?.value = String(index)
      }
      settingsRepository.update(settings: settings, with: tool.rawValue)
    }
  }

  // TODO: delete after if no need
  func update() {

  }
}
