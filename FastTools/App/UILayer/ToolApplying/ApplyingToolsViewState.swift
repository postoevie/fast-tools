//
//  PSToolsViewState.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

enum ApplyingToolsViewState {
  case overview
  case details(Tool)
  case apply(Tool)
}
