//
//  PSToolDetailsCellViewModel.swift
//  FastTools
//
//  Created by Igor Postoev on 19.06.2022.
//

protocol CellDetailsResponder {

  func edited(key: String, with value: String)
}

class PSToolDetailsCellViewModel: Hashable {

  let key: String
  var value: String
  let label: String
  let type: SettingType
  let updateResponder: CellDetailsResponder

  init(setting: ToolSetting, responder: CellDetailsResponder) {
    self.key = setting.key
    self.value = setting.value
    self.label = setting.label
    self.type = setting.type
    self.updateResponder = responder
  }

  func changed(_ value: String) {
    self.value = value
    updateResponder.edited(key: key, with: value)
  }

  func hash(into hasher: inout Hasher) {
    hasher.combine(key)
  }

  static func == (lhs: PSToolDetailsCellViewModel,
                  rhs: PSToolDetailsCellViewModel) -> Bool {
    return lhs.key == rhs.key
  }
}
