//
//  ToolDetailsUpdateResponder.swift
//  FastTools
//
//  Created by Igor Postoev on 15.05.2022.
//

protocol ToolDetailsUpdateResponder {

  func update()
}
