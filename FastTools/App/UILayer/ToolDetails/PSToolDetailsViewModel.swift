//
//  PSToolsOverviewViewModel.swift
//  FastTools
//
//  Created by Igor Postoev on 15.05.2022.
//

import UIKit

protocol PSToolDetailsView: UIViewController {
  func toolUpdated()
}

class PSToolDetailsViewModel: NSObject, CellDetailsResponder {

  weak var view: PSToolDetailsView!

  private var settings: [ToolSetting] {
    repository.getSettings(by: tool.rawValue)
  }

  private var tool: Tool!
  private let updateResponder: ToolDetailsUpdateResponder
  private let repository: ToolSettingsRepository

  init(responder: ToolDetailsUpdateResponder, repository: ToolSettingsRepository) {
    self.updateResponder = responder
    self.repository = repository
  }

  func update(with tool: Tool) {
    self.tool = tool
    view?.toolUpdated()
  }

  func getParametersCount() -> Int {
    return settings.filter { $0.visible }.count
  }

  func getItems() -> [PSToolDetailsCellViewModel] {
    return settings.filter { $0.visible }.map {
      PSToolDetailsCellViewModel(setting: $0,
                                 responder: self)
    }
  }

  // MARK: - CellDetailsResponder
  func edited(key: String, with value: String) {
    var settingsCopy = settings
    settingsCopy[key]?.value = value
    repository.update(settings: settingsCopy, with: tool.rawValue)
    updateResponder.update()
  }
}
