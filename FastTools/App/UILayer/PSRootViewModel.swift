//
//  PSRootViewModel.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import Combine

class PSRootViewModel {

  @Published var viewState: RootViewState = .tools
}
