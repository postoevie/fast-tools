//
//  PSRootViewController.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import UIKit
import Combine

class PSRootViewController: NiblessViewController {

  private let viewModel: PSRootViewModel
  private let makeToolsViewController: ControllerFactory

  private var subscribers = Set<AnyCancellable>()

  init(viewModel: PSRootViewModel, toolsFactory: @escaping ControllerFactory) {
    self.viewModel = viewModel
    self.makeToolsViewController = toolsFactory
    super.init()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .rootBackgroundColor
    subscribeToViewModel()
  }

  private func subscribeToViewModel() {
    viewModel.$viewState
      .receive(on: DispatchQueue.main)
      .sink { state in
        switch state {
        case .tools:
          self.presentToolsVC()
        }
      }
      .store(in: &subscribers)
  }

  func presentToolsVC() {
    let controller = self.makeToolsViewController()
    addAsFullScreen(child: controller)
  }
}
