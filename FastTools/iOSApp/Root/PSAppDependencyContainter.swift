//
//  PSAppDependencyContainter.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import UIKit

// main class for keeping long-term dependencies and creating top-level modules using factory methods
class PSAppDependencyContainter {

  let sharedUserSettings = "user settings"
  let sharedRootViewModel: PSRootViewModel

  var toolsContainer: PSToolsDependencyContainer!

  init() {
    self.sharedRootViewModel = PSRootViewModel()
  }

  func makeRootController() -> UIViewController {
    let dataStore = PSDefaultsToolsDatastore()
    let repository = PSToolSettingsRepository(store: dataStore)
    let viewModel = PSToolsApplyingViewModel(repository: repository)
    toolsContainer = PSToolsDependencyContainer(viewModel: viewModel)

    let toolsControllerFactory = {
      return self.toolsContainer.makeToolsController()
    }

    return PSRootViewController(viewModel: sharedRootViewModel,
                                toolsFactory: toolsControllerFactory)
  }
}
