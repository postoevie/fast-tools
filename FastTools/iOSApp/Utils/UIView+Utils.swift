//
//  UIView+Utils.swift
//  FastTools
//
//  Created by Igor Postoev on 15.05.2022.
//

import UIKit

extension UIView {
  
  func constraintToParentReadable() {
    guard let superview = superview else {
      return
    }
    let top = topAnchor.constraint(equalTo: superview.readableContentGuide.topAnchor)
    let leading = leadingAnchor.constraint(equalTo: superview.leadingAnchor)
    let bottom = superview.readableContentGuide.bottomAnchor.constraint(equalTo: bottomAnchor)
    let trailing = superview.trailingAnchor.constraint(equalTo: trailingAnchor)
    NSLayoutConstraint.activate([top, leading, bottom, trailing])
  }
}
