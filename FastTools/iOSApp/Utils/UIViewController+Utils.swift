//
//  UIViewController+Utils.swift
//  FastTools
//
//  Created by Igor Postoev on 07.05.2022.
//

import UIKit

extension UIViewController {
  
  func addAsFullScreen(child controller: UIViewController) {
    addChild(controller)
    view.addSubview(controller.view)
    let margins = view.layoutMarginsGuide
    controller.view.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
    controller.view.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
    controller.view.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
    controller.view.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
  }
}
