//
//  Closures.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import UIKit

typealias ControllerFactory = () -> UIViewController
