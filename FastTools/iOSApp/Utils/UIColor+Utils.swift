//
//  PSColors.swift
//  FastTools
//
//  Created by Igor Postoev on 10.05.2022.
//

import UIKit

extension UIColor {
  
  static let toolForegroundColor: UIColor = UIColor(named: "icon_background") ?? .clear
  
  static let rootBackgroundColor: UIColor = UIColor(named: "root_background") ?? .clear
  
  //thanks to https://www.hackingwithswift.com/example-code/uicolor/how-to-convert-a-hex-color-to-a-uicolor
  public convenience init?(hex: String) {
    let r, g, b, a: CGFloat
    
    guard hex.hasPrefix("#") else {
      return nil
    }
    
    let start = hex.index(hex.startIndex, offsetBy: 1)
    var hexColor = String(hex[start...])
    
    guard hexColor.count == 8 || hexColor.count == 6 else {
      return nil
    }

    if hexColor.count == 6 {
      hexColor += "FF"
    }

    let scanner = Scanner(string: hexColor)
    var hexNumber: UInt64 = 0
    
    if scanner.scanHexInt64(&hexNumber) {
      r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
      g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
      b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
      a = CGFloat(hexNumber & 0x000000ff) / 255
      
      self.init(red: r, green: g, blue: b, alpha: a)
      return
    }
    
    return nil
  }
  
  //https://stackoverflow.com/questions/26341008/how-to-convert-uicolor-to-hex-and-display-in-nslog
  func getHex() -> String {
    let components = cgColor.components
    let r: CGFloat = components?[0] ?? 0.0
    let g: CGFloat = components?[1] ?? 0.0
    let b: CGFloat = components?[2] ?? 0.0
    let a: CGFloat = components?[3] ?? 1.0
    return String(format: "#%02lX%02lX%02lX%02lX",
                  lroundf(Float(r * 255)),
                  lroundf(Float(g * 255)),
                  lroundf(Float(b * 255)),
                  lroundf(Float(a * 255)))
  }
}
