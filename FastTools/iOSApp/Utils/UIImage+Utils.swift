//
//  UIImage+Utils.swift
//  FastTools
//
//  Created by Igor Postoev on 10.05.2022.
//

import UIKit
import CoreGraphics

extension UIImage {
  
  func tinted(with color: UIColor) -> UIImage? {
    UIGraphicsBeginImageContext(size)
    
    guard let context = UIGraphicsGetCurrentContext(),
          let cgImage = cgImage else {
      return nil
    }
    let bounds = CGRect(origin: .zero, size: size)
    let verticalTransform = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: bounds.height)
    context.concatenate(verticalTransform)
    context.clip(to: bounds, mask: cgImage)
    
    color.setFill()
    context.fill(bounds)
   
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
}
