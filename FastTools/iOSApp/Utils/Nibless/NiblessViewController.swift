//
//  NiblessViewController+Utils.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import UIKit

open class NiblessViewController: UIViewController {
  
  public init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  @available(*, unavailable, message: "Storyboards are not supported")
  public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  @available(*, unavailable, message: "Coder init is not supported")
  public required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}
