//
//  NiblessTableCell.swift
//  FastTools
//
//  Created by Igor Postoev on 07.06.2022.
//

import UIKit

class NiblessTableCell: UITableViewCell {
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  }
  
  @available(*, unavailable, message: "Storyboards are not supported")
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
