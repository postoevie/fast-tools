//
//  NiblessNavigationViewController.swift
//  FastTools
//
//  Created by Igor Postoev on 07.05.2022.
//

import UIKit

class NiblessNavigationController: UINavigationController {
  
  public init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  @available(*, unavailable, message: "Storyboards are not supported")
  public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  @available(*, unavailable, message: "Coder init is not supported")
  public required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}
