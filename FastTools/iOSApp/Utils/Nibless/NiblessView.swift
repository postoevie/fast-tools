//
//  NiblessView.swift
//  FastTools
//
//  Created by Igor Postoev on 07.05.2022.
//

import UIKit

class NiblessView: UIView {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  @available(*, unavailable, message: "Storyboards are not supported")
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
