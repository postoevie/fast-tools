//
//  PSSwitch.swift
//  FastTools
//
//  Created by Igor Postoev on 20.06.2022.
//

import Combine
import UIKit
import Foundation

// Thanks to Matt Neuburg

// TODO: learn UIcontrols UIMenu UIActions
class PSSwitch: UISwitch {

  @Published var isOnPublisher = false

  init() {
    super.init(frame: .zero)
    isOnPublisher = isOn
    let action = UIAction { [unowned self] _ in
      self.isOnPublisher = self.isOn
    }
    addAction(action, for: .valueChanged)
  }

  required init?(coder: NSCoder) {
    fatalError("nibs aren't supported")
  }
}
