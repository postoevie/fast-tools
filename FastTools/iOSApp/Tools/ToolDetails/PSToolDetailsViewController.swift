//
//  PSToolDetailsViewController.swift
//  FastTools
//
//  Created by Igor Postoev on 08.03.2022.
//

import UIKit

// отображает список настроек инструмента
// под списком настроек кнопка применения инструмента
class PSToolDetailsViewController: NiblessViewController, PSToolDetailsView {

  private lazy var dataSource: UITableViewDiffableDataSource<DetailsSection, PSToolDetailsCellViewModel> = {
    // swiftlint:disable:next line_length
    let dataSource = UITableViewDiffableDataSource<DetailsSection, PSToolDetailsCellViewModel>(tableView: tableView) { tableView, indexPath, viewModel in
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell",
                                                     for: indexPath) as? PSToolDetailsCell else {
        return PSToolDetailsCell()
      }
      cell.viewModel = viewModel
      return cell
    }
    return dataSource
  }()

  private lazy var tableView: UITableView = {
    let table = UITableView()
    table.translatesAutoresizingMaskIntoConstraints = false
    table.directionalLayoutMargins.leading = 0
    table.allowsSelection = false
    table.register(PSToolDetailsCell.self, forCellReuseIdentifier: "cell")
    return table
  }()

  private lazy var executeButton: UIButton = {
    let button = UIButton()
    button.backgroundColor = .toolForegroundColor
    button.setTitle("RUN", for: .normal)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.layer.cornerRadius = 16
    return button
  }()

  let viewModel: PSToolDetailsViewModel

  init(viewModel: PSToolDetailsViewModel) {
    self.viewModel = viewModel
    super.init()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(tableView)
    view.addSubview(executeButton)
    constraintSubviews()
  }

  func constraintSubviews() {
    let guide = view.layoutMarginsGuide
    NSLayoutConstraint.activate(
      [
        tableView.topAnchor.constraint(equalTo: guide.topAnchor),
       tableView.leftAnchor.constraint(equalTo: guide.leftAnchor),
       tableView.rightAnchor.constraint(equalTo: guide.rightAnchor),
       executeButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
       executeButton.leftAnchor.constraint(equalTo: guide.leftAnchor),
       executeButton.rightAnchor.constraint(equalTo: guide.rightAnchor),
       executeButton.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
       executeButton.heightAnchor.constraint(equalToConstant: 100)
      ]
    )
  }

  override func viewWillAppear(_ animated: Bool) {
    tableView.reloadData()
  }

  func toolUpdated() {
    var snapshot = NSDiffableDataSourceSnapshot<DetailsSection, PSToolDetailsCellViewModel>()
    snapshot.appendSections([.main])
    snapshot.appendItems(viewModel.getItems())
    dataSource.apply(snapshot, animatingDifferences: false)
  }
}
