//
//  PSToolDetailsCellViewCoordinator.swift
//  FastTools
//
//  Created by Igor Postoev on 20.06.2022.
//

import UIKit
import Combine

class PSToolDetailsCellViewCoordinator {

  var viewModel: PSToolDetailsCellViewModel!

  private var subscriptions = Set<AnyCancellable>()

  func createView() -> UIView {
    subscriptions.forEach {
      $0.cancel()
    }
    subscriptions = []
    var view: UIView
    switch viewModel.type {
    case .color:
      view = createColorWell()
    case .bool:
      view = createSwitch()
    default:
      view = createTextField()
    }
    return view
  }

  private func createTextField() -> UITextField {
    let textField = UITextField()
    textField.text = viewModel.value
    textField
      .publisher(for: \.text)
      .map { $0 ?? "" }
      .sink {
        self.viewModel.changed($0)
      }
      .store(in: &subscriptions)
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }

  private func createSwitch() -> UISwitch {
    let switchView = PSSwitch()
    switchView.isOn = Bool(viewModel.value) ?? false
    switchView
      .$isOnPublisher
      .map { String($0) }
      .sink {
        self.viewModel.changed($0)
      }
      .store(in: &subscriptions)
    switchView.translatesAutoresizingMaskIntoConstraints = false
    return switchView
  }

  private func createColorWell() -> UIColorWell {
    let colorWell = UIColorWell()
    colorWell.selectedColor = UIColor(hex: viewModel.value)
    colorWell
      .publisher(for: \.selectedColor)
      .map { $0?.getHex() ?? "" }
      .sink {
        self.viewModel.changed($0)
      }
      .store(in: &subscriptions)
    colorWell.translatesAutoresizingMaskIntoConstraints = false
    return colorWell
  }

  func set(value: String, type: SettingType, in view: UIView) {
    switch type {
    case .bool:
      (view as? UISwitch)?.isOn = Bool(value) ?? false
    case .color:
      (view as? UIColorWell)?.selectedColor = UIColor(hex: value)
    default:
      (view as? UITextField)?.text = value
    }
  }
}
