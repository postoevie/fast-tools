//
//  PSToolDetailsCell.swift
//  FastTools
//
//  Created by Igor Postoev on 07.06.2022.
//
import UIKit

class PSToolDetailsCell: NiblessTableCell {

  private let viewCoordinator = PSToolDetailsCellViewCoordinator()

  var viewModel: PSToolDetailsCellViewModel! {
    didSet {
      keyLabel.text = viewModel.label
      viewCoordinator.viewModel = viewModel
      if viewModel.type == oldValue?.type,
         let innerView = valueViewContainer.subviews.first {
        viewCoordinator.set(value: viewModel.value,
                            type: viewModel.type,
                            in: innerView)
        return
      }
      let valueView = viewCoordinator.createView()
      addValueSubview(valueView)
    }
  }

  private var keyLabel = UILabel()
  private var valueViewContainer = UIView()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    separatorInset.left = 0
    contentView.addSubview(keyLabel)
    contentView.addSubview(valueViewContainer)
    keyLabel.translatesAutoresizingMaskIntoConstraints = false
    valueViewContainer.translatesAutoresizingMaskIntoConstraints = false
    setupConstraints()
  }

  private func setupConstraints() {
    let guide = contentView.layoutMarginsGuide
    NSLayoutConstraint.activate([
      keyLabel.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
      keyLabel.topAnchor.constraint(greaterThanOrEqualTo: guide.topAnchor),
      keyLabel.bottomAnchor.constraint(greaterThanOrEqualTo: guide.bottomAnchor),
      keyLabel.trailingAnchor.constraint(equalTo: valueViewContainer.leadingAnchor),
      valueViewContainer.widthAnchor.constraint(equalToConstant: 150),
      valueViewContainer.topAnchor.constraint(equalTo: guide.topAnchor),
      valueViewContainer.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
      guide.trailingAnchor.constraint(equalTo: valueViewContainer.trailingAnchor)
    ])
  }

  private func addValueSubview(_ view: UIView) {
    valueViewContainer.subviews.forEach {
      $0.removeFromSuperview()
    }
    valueViewContainer.addSubview(view)
    NSLayoutConstraint.activate(
      [
        view.topAnchor.constraint(equalTo: valueViewContainer.topAnchor),
        view.bottomAnchor.constraint(equalTo: valueViewContainer.bottomAnchor),
        view.widthAnchor.constraint(lessThanOrEqualTo: valueViewContainer.widthAnchor)
      ]
    )
  }
}
