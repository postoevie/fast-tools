//
//  PSToolsOverviewView.swift
//  FastTools
//
//  Created by Igor Postoev on 07.05.2022.
//

import UIKit

class PSToolsOverviewCell: UICollectionViewCell, CAAnimationDelegate {

  private var imageView = UIImageView()
  private var label = UILabel()
  private var animator: UIViewPropertyAnimator?
  var canWiggling: Bool = false

  override init(frame: CGRect) {
    super.init(frame: .zero)
    layer.cornerRadius = 10
    layer.borderWidth = 2
    contentView.addSubview(imageView)
    contentView.addSubview(label)
    setupConstraints()
  }

  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    if canWiggling {
      startWiggling()
    }
  }

  private func setupConstraints() {
    imageView.translatesAutoresizingMaskIntoConstraints = false
    label.translatesAutoresizingMaskIntoConstraints = false
    imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10).isActive = true
  }

  func setup(with item: OverviewItem) {
    var image = UIImage(named: item.imageName)
    guard let tintedImage = image?.tinted(with: .toolForegroundColor),
          let cgImage = tintedImage.cgImage else {
      return
    }
    image = UIImage(cgImage: cgImage,
                    scale: tintedImage.scale * 1.8,
                    orientation: tintedImage.imageOrientation)
    imageView.image = image
    imageView.contentMode = .scaleAspectFit
    label.text = item.title
  }

  func startWiggling() {
    let anim = CAKeyframeAnimation(keyPath:#keyPath(CALayer.transform))
    anim.repeatCount = .greatestFiniteMagnitude
    anim.values = [Float.pi/30, -Float.pi/30]
    anim.valueFunction = CAValueFunction(name: .rotateZ)
    anim.autoreverses = true
    anim.delegate = self
    imageView.layer.add(anim, forKey: nil)
  }

  func stopWiggling() {
    imageView.layer.removeAllAnimations()
  }

  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
    if canWiggling {
      startWiggling()
    }
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
