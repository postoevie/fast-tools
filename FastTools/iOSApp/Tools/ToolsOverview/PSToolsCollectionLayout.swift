//
//  PSToolsCollectionLayout.swift
//  FastTools
//
//  Created by Igor Postoev on 10.05.2022.
//

import UIKit

class PSToolsCollectionLayout: UICollectionViewCompositionalLayout {

  convenience init() {
    let sectionProvider: UICollectionViewCompositionalLayoutSectionProvider = { sectionIndex, _ in
      let rows = 4
      let columns = 2
      let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                            heightDimension: .fractionalHeight(1.0))
      let item = NSCollectionLayoutItem(layoutSize: itemSize)
      item.contentInsets.top = 10
      let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                             heightDimension: .fractionalHeight(1.0 / CGFloat(rows)))
      let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                     subitem: item,
                                                     count: sectionIndex == 0 ? columns : 1)
      group.interItemSpacing = .fixed(10)
      let section = NSCollectionLayoutSection(group: group)
      return section
    }
    self.init(sectionProvider: sectionProvider)
  }

  override init(sectionProvider: @escaping UICollectionViewCompositionalLayoutSectionProvider) {
    super.init(sectionProvider: sectionProvider)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
