//
//  PSToolsOverviewViewController.swift
//  FastTools
//
//  Created by Igor Postoev on 08.03.2022.
//

import UIKit

class PSToolsOverviewViewController: NiblessViewController,
                                     UICollectionViewDataSource,
                                     UICollectionViewDelegate {

  private var movingIndex: IndexPath?
  private var canRearrange: Bool = false

  let viewModel: PSToolsOverviewViewModel

  init(viewModel: PSToolsOverviewViewModel) {
    self.viewModel = viewModel
    super.init()
  }

  private lazy var collectionView: UICollectionView = {
    var view = UICollectionView(frame: .zero,
                                collectionViewLayout: PSToolsCollectionLayout())
    view.translatesAutoresizingMaskIntoConstraints = false
    view.dataSource = self
    view.delegate = self
    view.isScrollEnabled = false
    view.register(PSToolsOverviewCell.self, forCellWithReuseIdentifier: "cell")
    return view
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigationBar()
    view.addSubview(collectionView)
    setupConstraints()
  }

  private func setupNavigationBar() {
    let listImage = UIImage(systemName: "list.number")
    let action = UIAction { [weak self] _ in
      if self?.canRearrange ?? false {
        self?.exitRearrangeState()
      } else {
        self?.enterRearrangeState()
      }
    }
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: nil,
                                                        image: listImage,
                                                        primaryAction: action,
                                                        menu: nil)
    navigationItem.rightBarButtonItem?.tintColor = .toolForegroundColor
  }

  private func setupConstraints() {
    let top = collectionView.topAnchor.constraint(equalTo: view.readableContentGuide.topAnchor)
    let leading = collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor,
                                                          constant: 10)
    let bottom = view.readableContentGuide.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 20)
    let trailing = view.trailingAnchor.constraint(equalTo: collectionView.trailingAnchor,
                                                  constant: 10)
    NSLayoutConstraint.activate([top, leading, bottom, trailing])
  }

  // MARK: - UICollectionViewDataSource
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 2
  }

  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    return section == 0 ? viewModel.getItemsCount() - 1 : 1
  }

  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                        for: indexPath) as? PSToolsOverviewCell else {
      return PSToolsOverviewCell()
    }
    // due to bottom element is external section
    let index = getFlatIndex(for: indexPath)
    if let item = viewModel.getItem(index: index) {
      cell.setup(with: item)
    }
    return cell
  }

  func collectionView(_ collectionView: UICollectionView,
                      didSelectItemAt indexPath: IndexPath) {
    let index = getFlatIndex(for: indexPath)
    viewModel.selected(in: index)
  }

  func collectionView(_ collectionView: UICollectionView,
                      contextMenuConfigurationForItemAt indexPath: IndexPath,
                      point: CGPoint) -> UIContextMenuConfiguration? {
    return UIContextMenuConfiguration(identifier: nil,
                                      previewProvider: nil) { _ in
      let configureAction = UIAction(title: "Config") { [weak self] _ in
        guard let someSelf = self else {
          return
        }
        let flatIndex = someSelf.getFlatIndex(for: indexPath)
        someSelf.viewModel.drillToDetails(in: flatIndex)
      }
      let moveAction = UIAction(title: "Move") { [weak self] _ in
        self?.enterMovingState(in: indexPath)
      }
      return UIMenu(title: "", children: [configureAction, moveAction])
    }
  }

  func collectionView(_ collectionView: UICollectionView,
                      canMoveItemAt indexPath: IndexPath) -> Bool {
    return movingIndex == indexPath || canRearrange
  }

  func collectionView(_ collectionView: UICollectionView,
                      moveItemAt sourcePath: IndexPath,
                      to destinationPath: IndexPath) {
    let cell = collectionView.cellForItem(at: sourcePath) as? PSToolsOverviewCell
    cell?.stopWiggling()
    exitMovingState(in: destinationPath)

    let sourceIndex = getFlatIndex(for: sourcePath)
    let destinationIndex = getFlatIndex(for: destinationPath)
    viewModel.moved(sourceIndex, destinationIndex)

    // TODO: make smoother
    collectionView.reloadData()
  }

  private func getFlatIndex(for indexPath: IndexPath) -> Int {
    return indexPath.section * (viewModel.getItemsCount() - 1) + indexPath.item
  }

  private func enterRearrangeState() {
    collectionView.visibleCells.forEach {
      ($0 as? PSToolsOverviewCell)?.canWiggling = true
      ($0 as? PSToolsOverviewCell)?.startWiggling()
    }
    canRearrange = true
    collectionView.allowsSelection = false
    mountPanGesture()
    navigationItem.rightBarButtonItem?.tintColor = .green
  }

  private func exitRearrangeState() {
    collectionView.visibleCells.forEach {
      ($0 as? PSToolsOverviewCell)?.canWiggling = false
      ($0 as? PSToolsOverviewCell)?.stopWiggling()
    }
    canRearrange = false
    collectionView.allowsSelection = true
    unMountPanGesture()
    navigationItem.rightBarButtonItem?.tintColor = .toolForegroundColor
  }

  private func enterMovingState(in indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as? PSToolsOverviewCell
    cell?.startWiggling()
    movingIndex = indexPath
    collectionView.allowsSelection = false
    mountPanGesture()
    navigationItem.rightBarButtonItem?.isEnabled = false
  }

  private func exitMovingState(in indexPath: IndexPath) {
    guard !canRearrange else {
      return
    }
    let cell = collectionView.cellForItem(at: indexPath) as? PSToolsOverviewCell
    cell?.stopWiggling()
    movingIndex = nil
    collectionView.allowsSelection = true
    unMountPanGesture()
    navigationItem.rightBarButtonItem?.isEnabled = true
  }

  // MARK: - UIPanGestureRecognizer

  private func mountPanGesture() {
    let panGesture = UIPanGestureRecognizer(target: self,
                                            action: #selector(handlePanGesture(_:)))
    // since collection view obtains multiple pan gestures specify a name
    panGesture.name = "pan"
    collectionView.addGestureRecognizer(panGesture)
  }

  private func unMountPanGesture() {
    if let panRecognizer = getPanRecognizer() {
      collectionView.removeGestureRecognizer(panRecognizer)
    }
  }

  private func getPanRecognizer() -> UIGestureRecognizer? {
    return collectionView.gestureRecognizers?.first {
      $0.name == "pan"
    }
  }

  @objc
  private func handlePanGesture(_ gesture: UIPanGestureRecognizer) {
    switch gesture.state {
    case .began:
      let point = gesture.location(in: gesture.view)
      if let path = collectionView.indexPathForItem(at: point) {
        collectionView.beginInteractiveMovementForItem(at: path)
      }
    case .changed:
      let point = gesture.location(in: gesture.view)
      collectionView.updateInteractiveMovementTargetPosition(point)
    case .ended:
      collectionView.endInteractiveMovement()
    default:
      collectionView.cancelInteractiveMovement()
    }
  }
}
