//
//  PSToolViewController.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import UIKit
import Combine

// вью-контейнер для PSToolsOverviewViewController, PSToolDetailsViewController
class PSToolsApplyingViewController: NiblessNavigationController {

  let viewModel: PSToolsApplyingViewModel
  let overviewViewController: PSToolsOverviewViewController
  let detailsViewController: PSToolDetailsViewController

  private var subscribers = Set<AnyCancellable>()

  init(viewModel: PSToolsApplyingViewModel,
       detailsController: PSToolDetailsViewController,
       overviewController: PSToolsOverviewViewController) {
    self.viewModel = viewModel
    self.detailsViewController = detailsController
    self.overviewViewController = overviewController
    super.init()
  }

  override func viewDidLoad() {
    subscribeToViewState()
  }

  private func subscribeToViewState() {
    viewModel.$viewState
      .receive(on: DispatchQueue.main)
      .sink { state in
        self.navigateTo(state)
      }
      .store(in: &subscribers)
  }

  private func navigateTo(_ state: ApplyingToolsViewState) {
    switch state {
    case .overview:
      pushViewController(overviewViewController, animated: true)
    case .details(let tool):
      detailsViewController.viewModel.update(with: tool)
      pushViewController(detailsViewController, animated: true)
    case .apply:
      break
    }
  }
}
