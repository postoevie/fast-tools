//
//  PSToolsDependencyContainer.swift
//  FastTools
//
//  Created by Igor Postoev on 07.03.2022.
//

import UIKit

class PSToolsDependencyContainer {

  let viewModel: PSToolsApplyingViewModel

  init(viewModel: PSToolsApplyingViewModel) {
    self.viewModel = viewModel
  }

  func makeToolsController() -> UIViewController {
    return PSToolsApplyingViewController(viewModel: viewModel,
                                         detailsController: createDetailsController(),
                                         overviewController: createOverviewController())
  }

  private func createOverviewController() -> PSToolsOverviewViewController {
    let overviewViewModel = PSToolsOverviewViewModel(dataSource: viewModel)
    let controller = PSToolsOverviewViewController(viewModel: overviewViewModel)
    return controller
  }

  private func createDetailsController() -> PSToolDetailsViewController {
    let detailsViewModel = PSToolDetailsViewModel(responder: viewModel, repository: viewModel.settingsRepository)
    let controller = PSToolDetailsViewController(viewModel: detailsViewModel)
    detailsViewModel.view = controller
    return controller
  }
}
